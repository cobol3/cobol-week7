       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. NUTHJAREE

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
   

       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE VALUE  HIGH-VALUE.
           05 COURSE-CODE PIC X(6).
           05 COURSE-NAME PIC X(50).
           05 CREDIT PIC 9.
           05 GRADE PIC X(2).

       FD  AVG-GRADE-FILE.
       01 AVG-DETAIL.
           05 AVG-GRADES PIC 9(1)V9(3).
           05 AVG-SCI-GRADES PIC 9(1)V9(3).
           05 AVG-CS-GRADES PIC 9(1)V9(3).

       WORKING-STORAGE SECTION. 
       01  GRADE-TYPE PIC 9V9.
       
       01  GRADE-DET.
           05 SUMJECT-COURSE-CODE PIC X(6).
           05 SUMJECT-COURSE-NAME PIC X(50).
           05 SUMJECT-CREDIT PIC 9.
           05 SUMJECT-GRADE PIC X(2).

       01  COURSE-CODE-SCI-DETAIL REDEFINES GRADE-DET.
           05 COURSE-CODE-SCI PIC A.
       01  COURSE-CODE-CS-DETAIL REDEFINES GRADE-DET.
           05 COURSE-CODE-CS PIC AA.
       
      *AVG-GRADE
       01  ALL-CREDIT PIC 999.
       01  AVG-GRADE PIC 9(3)V9(3).
       01  RESULT-AVG-GRADE PIC 9(1)V9(3).
       
      *AVG-GRADE-SCI
       01  ALL-SCI-CREDIT PIC 999.
       01  AVG-SCI-GRADE PIC 9(3)V9(3).
       01  RESULT-AVG-SCI-GRADE PIC 9(1)V9(3).

      *AVG-GRADE-CS
       01  ALL-CS-CREDIT PIC 999.
       01  AVG-CS-GRADE PIC 9(3)V9(3).
       01  RESULT-AVG-CS-GRADE PIC 9(1)V9(3).
    
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 

           PERFORM UNTIL END-OF-GRADE-FILE 

              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE
              END-READ

              IF NOT END-OF-GRADE-FILE 
                 PERFORM 001-PROCESS THRU 001-EXIT 

      *           AVG-GRADE
                  COMPUTE ALL-CREDIT = ALL-CREDIT + CREDIT 
                  COMPUTE AVG-GRADE = AVG-GRADE +( CREDIT * GRADE-TYPE )
                  COMPUTE RESULT-AVG-GRADE = AVG-GRADE / ALL-CREDIT 
                  
      *           AVG-GRADE-SCI
                  IF COURSE-CODE-SCI IS EQUAL TO "3" THEN
                    COMPUTE ALL-SCI-CREDIT = ALL-SCI-CREDIT + CREDIT 
                    COMPUTE AVG-SCI-GRADE = AVG-SCI-GRADE + 
                    (CREDIT  * GRADE-TYPE )
                    COMPUTE RESULT-AVG-SCI-GRADE = AVG-SCI-GRADE 
                    / ALL-SCI-CREDIT
                   END-IF

      *            AVG-GRADE-CS
                   IF COURSE-CODE-CS IS EQUAL TO "31" THEN
                    COMPUTE ALL-CS-CREDIT = ALL-CS-CREDIT + CREDIT 
                    COMPUTE AVG-CS-GRADE = AVG-CS-GRADE + 
                    (CREDIT  * GRADE-TYPE )
                    COMPUTE RESULT-AVG-CS-GRADE = AVG-CS-GRADE 
                    / ALL-CS-CREDIT
                   END-IF

              END-IF

           END-PERFORM
           
           CLOSE GRADE-FILE 

           OPEN OUTPUT  AVG-GRADE-FILE 
           MOVE RESULT-AVG-GRADE TO AVG-GRADES  
           MOVE RESULT-AVG-SCI-GRADE TO AVG-SCI-GRADES
           MOVE RESULT-AVG-CS-GRADE  TO AVG-CS-GRADES
           WRITE AVG-DETAIL 
           CLOSE AVG-GRADE-FILE 

           DISPLAY "============================="
           DISPLAY "AVG-GRADE: " RESULT-AVG-GRADE 
           DISPLAY "============================="
           DISPLAY "AVG-SCI-GRADE: " RESULT-AVG-SCI-GRADE 
           DISPLAY "============================="
           DISPLAY "AVG-CS-GRADE: " RESULT-AVG-CS-GRADE 
           DISPLAY "============================="
           GOBACK 
           .
           
       001-PROCESS.
           MOVE COURSE-CODE TO SUMJECT-COURSE-CODE 
           EVALUATE TRUE
              WHEN GRADE IS EQUAL TO "A " MOVE 4 TO GRADE-TYPE
              WHEN GRADE IS EQUAL TO "B+" MOVE 3.5 TO GRADE-TYPE
              WHEN GRADE IS EQUAL TO "B " MOVE 3 TO GRADE-TYPE
              WHEN GRADE IS EQUAL TO "C+" MOVE 2.5 TO GRADE-TYPE
              WHEN GRADE IS EQUAL TO "C " MOVE 2 TO GRADE-TYPE
              WHEN GRADE IS EQUAL TO "D+" MOVE 1.5 TO GRADE-TYPE
              WHEN GRADE IS EQUAL TO "D " MOVE 1 TO GRADE-TYPE
              WHEN OTHER  MOVE 0 TO GRADE-TYPE
           END-EVALUATE
           .
       001-EXIT.
           EXIT 
           .
